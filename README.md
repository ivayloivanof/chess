chess
============

This is a very simple chess game written in Java. 

A Java installation is required. 

The user can play against the computer player only (no human vs human). This game have 5 levels of difficulties.

This game was written in 2017 year for educational purposes. In this game have best practices and algorithms in general, so the code isn't that nice and easy to follow and the packages weren't organized well.
