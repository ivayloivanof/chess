package chess;

import chess.game.Game;
import chess.game.GameData;
import chess.move.Move;
import chess.move.MoveSearcher;
import chess.pane.AboutPane;
import chess.pane.PreferencesPane;
import chess.pane.PromotionPane;
import chess.piece.Piece;
import chess.position.Position;
import chess.resource.Resource;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainChess extends JFrame implements MouseListener {

	private static final int SIZE_HEIGHT = 525;
	private static final int SIZE_WIDTH = 725;

	JPanel east_pane;
	boolean piece_selected;
	boolean is_white;
	MoveSearcher move_searcher;
	Game game;
	JLabel newGame, quit, about, history, first, prev, next, last;
	PreferencesPane play_options;
	boolean castling;
	PromotionPane promotion_pane;
	List<Position> history_positions = new ArrayList<Position>();
	int history_count;
	private Color backgroundColor = Color.decode("#c4a362");
	private Position position;
	private ChessBoardPane boardPane;
	private int state;
	private HistoryBoardPane historyBoardPane;
	private Resource resource = new Resource();
	private Move move = new Move();
	private JPanel mainPane = new JPanel(new BorderLayout());
	private Map<Integer, Image> images = new HashMap<Integer, Image>();
	private Map<Integer, Icon> iconImages = new HashMap<Integer, Icon>();

	public MainChess() {
		super(GameData.GAME_NAME + GameData.VERSION);
		setContentPane(this.mainPane);
		this.position = new Position();
		promotion_pane = new PromotionPane(this);

		loadMenuIcons();
		loadBoardImages();

		this.boardPane = new ChessBoardPane();

		this.mainPane.add(createMenuPane(), BorderLayout.WEST);
		this.mainPane.add(this.boardPane, BorderLayout.CENTER);
		this.mainPane.setBackground(this.backgroundColor);
		createEastPane();

		pack();
		Dimension size = getSize();
		size.height = SIZE_HEIGHT;
		size.width = SIZE_WIDTH;
		setSize(size);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				quit();
			}
		});
	}

	public static void main(String[] args) {
		// TODO code application logic here
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					boolean nimbusFound = false;
					for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
						if (info.getName().equals("Nimbus")) {
							UIManager.setLookAndFeel(info.getClassName());
							nimbusFound = true;
							break;
						}
					}
					if (!nimbusFound) {
						int option = JOptionPane.showConfirmDialog(null,
								"Nimbus Look And Feel not found\n" +
										"Do you want to proceed?",
								"Warning", JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE);
						if (option == JOptionPane.NO_OPTION) {
							System.exit(0);
						}
					}
					MainChess mcg = new MainChess();
					// mcg.pack();
					mcg.setLocationRelativeTo(null);
					mcg.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
					mcg.setResizable(false);
					mcg.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getStackTrace());
					e.printStackTrace();
				}
			}
		});
	}

	private JPanel createMenuPane() {
		newGame = new JLabel(iconImages.get(GameData.NEW_BUTTON));
		about = new JLabel(iconImages.get(GameData.ABOUT_BUTTON));
		history = new JLabel(iconImages.get(GameData.HISTORY_BUTTON));
		quit = new JLabel(iconImages.get(GameData.QUIT_BUTTON));

		newGame.addMouseListener(this);
		about.addMouseListener(this);
		history.addMouseListener(this);
		quit.addMouseListener(this);

		JPanel pane = new JPanel(new GridLayout(4, 1));
		pane.add(newGame);
		pane.add(history);
		pane.add(about);
		pane.add(quit);
		pane.setBackground(backgroundColor);
		JPanel menu_pane = new JPanel(new BorderLayout());
		menu_pane.setBackground(backgroundColor);
		menu_pane.add(pane, BorderLayout.SOUTH);
		menu_pane.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 0));
		return menu_pane;
	}

	public void createEastPane() {
		east_pane = new JPanel(new BorderLayout());
		historyBoardPane = new HistoryBoardPane();

		JPanel pane = new JPanel(new GridLayout(1, 4));
		first = new JLabel(iconImages.get(GameData.FIRST_BUTTON));
		prev = new JLabel(iconImages.get(GameData.PREV_BUTTON));
		next = new JLabel(iconImages.get(GameData.NEXT_BUTTON));
		last = new JLabel(iconImages.get(GameData.LAST_BUTTON));

		pane.add(first);
		pane.add(prev);
		pane.add(next);
		pane.add(last);

		JPanel pane2 = new JPanel();
		pane2.setLayout(new BoxLayout(pane2, BoxLayout.Y_AXIS));
		pane2.add(historyBoardPane);
		pane2.add(pane);

		east_pane.add(pane2, BorderLayout.SOUTH);
		east_pane.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
		east_pane.setBackground(backgroundColor);
		east_pane.setVisible(false);
		mainPane.add(east_pane, BorderLayout.EAST);

		pane.setBorder(BorderFactory.createEmptyBorder(0, 14, 0, 14));
		pane.setBackground(backgroundColor);

		first.addMouseListener(this);
		prev.addMouseListener(this);
		next.addMouseListener(this);
		last.addMouseListener(this);
	}

	public void newGame() {
		if (!east_pane.isVisible()) {
			east_pane.setVisible(true);
			pack();
			setLocationRelativeTo(null);
		}
		is_white = play_options.getWhiteButton().isSelected();
		move.setSourceLocation(-1);
		move.setDestination(-1);
		position = new Position();
		position.initialize(is_white);
		game = new Game(position);
		loadPieceImages();
		promotion_pane.setIcons(is_white);
		boardPane.repaint();
		if (is_white) state = GameData.HUMAN_MOVE;
		else state = GameData.COMPUTER_MOVE;
		castling = false;
		history_positions.clear();
		history_count = 0;
		newHistoryPosition();
		move_searcher.setLevel(play_options.getLevelSlider().getValue());
		play();
	}

	public void play() {
		Thread t = new Thread() {
			public void run() {
				while (true) {
					switch (state) {
						case GameData.HUMAN_MOVE:
							break;
						case GameData.COMPUTER_MOVE:
							if (gameEnded(GameData.COMPUTER)) {
								state = GameData.GAME_ENDED;
								break;
							}
							move = move_searcher.alphaBeta(GameData.COMPUTER, position,
									Integer.MIN_VALUE, Integer.MAX_VALUE, play_options.getLevelSlider().getValue()).getLastMove();
							state = GameData.PREPARE_ANIMATION;
							break;
						case GameData.PREPARE_ANIMATION:
							prepareAnimation();
							break;
						case GameData.ANIMATING:
							animate();
							break;
						case GameData.GAME_ENDED:
							return;
					}
					try {
						Thread.sleep(3);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}

	public boolean gameEnded(int player) {
		int result = game.getResult(player);
		boolean end_game = false;
		String color = "";
		if (player == GameData.COMPUTER) {
			color = (is_white) ? "White" : "Black";
		} else color = (is_white) ? "Black" : "White";
		if (result == GameData.CHECKMATE) {
			showEndGameResult(color + " wins by CHECKMATE");
			end_game = true;
		} else if (result == GameData.DRAW) {
			showEndGameResult("DRAW");
			end_game = true;
		}
		return end_game;
	}

	public void showEndGameResult(String message) {
		int option = JOptionPane.showOptionDialog(null,
				message, "Game Over", 0, JOptionPane.PLAIN_MESSAGE,
				null, new Object[]{"Play again", "Cancel"}, "Play again");
		if (option == 0) {
			play_options.setVisible(true);
		}
	}

	public void showNewGameWarning() {
		JOptionPane.showMessageDialog(null,
				"Start a new game after I made my move.\n",
				"Message", JOptionPane.PLAIN_MESSAGE);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Object source = e.getSource();
		if (source == quit) {
			quit();
		} else if (source == newGame) {
			if (state == GameData.COMPUTER_MOVE) {
				showNewGameWarning();
				return;
			}
			if (play_options == null) {
				play_options = new PreferencesPane(this);
				move_searcher = new MoveSearcher(this);
			}
			play_options.setVisible(true);
		} else if (source == about) {
			AboutPane.createAndShowUI();
		} else if (source == history) {
			east_pane.setVisible(!east_pane.isVisible());
			pack();
			setLocationRelativeTo(null);
		} else if (source == first) {
			history_count = 0;
			historyBoardPane.repaint();
		} else if (source == prev) {
			if (history_count > 0) {
				history_count--;
				historyBoardPane.repaint();
			}
		} else if (source == next) {
			if (history_count < history_positions.size() - 1) {
				history_count++;
				historyBoardPane.repaint();
			}
		} else if (source == last) {
			history_count = history_positions.size() - 1;
			historyBoardPane.repaint();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Object source = e.getSource();
		if (source == newGame) {
			newGame.setIcon(iconImages.get(GameData.NEW_BUTTON2));
		} else if (source == about) {
			about.setIcon(iconImages.get(GameData.ABOUT_BUTTON2));
		} else if (source == history) {
			history.setIcon(iconImages.get(GameData.HISTORY_BUTTON2));
		} else if (source == quit) {
			quit.setIcon(iconImages.get(GameData.QUIT_BUTTON2));
		} else if (source == first) {
			first.setIcon(iconImages.get(GameData.FIRST_BUTTON2));
		} else if (source == prev) {
			prev.setIcon(iconImages.get(GameData.PREV_BUTTON2));
		} else if (source == next) {
			next.setIcon(iconImages.get(GameData.NEXT_BUTTON2));
		} else if (source == last) {
			last.setIcon(iconImages.get(GameData.LAST_BUTTON2));
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Object source = e.getSource();
		if (source == newGame) {
			newGame.setIcon(iconImages.get(GameData.NEW_BUTTON));
		} else if (source == about) {
			about.setIcon(iconImages.get(GameData.ABOUT_BUTTON));
		} else if (source == history) {
			history.setIcon(iconImages.get(GameData.HISTORY_BUTTON));
		} else if (source == quit) {
			quit.setIcon(iconImages.get(GameData.QUIT_BUTTON));
		} else if (source == first) {
			first.setIcon(iconImages.get(GameData.FIRST_BUTTON));
		} else if (source == prev) {
			prev.setIcon(iconImages.get(GameData.PREV_BUTTON));
		} else if (source == next) {
			next.setIcon(iconImages.get(GameData.NEXT_BUTTON));
		} else if (source == last) {
			last.setIcon(iconImages.get(GameData.LAST_BUTTON));
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	public boolean validMove(int destination) {
		int source = move.getSourceLocation();
		int destination_square = position.getBoard()[destination];
		if (destination_square == GameData.ILLEGAL) return false;
		if (!game.safeMove(GameData.HUMAN, source, destination)) return false;
		boolean valid = false;
		int piece_value = this.getPosition().getHumanPieces()[position.getBoard()[source]].getValue();
		switch (piece_value) {
			case GameData.PIECE_PAWN:
				if (destination == source - 10 && destination_square == GameData.EMPTY) valid = true;
				if (destination == source - 20 && position.getBoard()[source - 10] == GameData.EMPTY &&
						destination_square == GameData.EMPTY && source > 80) valid = true;
				if (destination == source - 9 && destination_square < 0) valid = true;
				if (destination == source - 11 && destination_square < 0) valid = true;
				break;
			case GameData.PIECE_KNIGHT:
			case GameData.PIECE_KING:
				if (piece_value == GameData.PIECE_KING) valid = checkCastling(destination);
				int[] destinations = null;
				if (piece_value == GameData.PIECE_KNIGHT)
					destinations = new int[]{source - 21, source + 21, source + 19, source - 19,
							source - 12, source + 12, source - 8, source + 8};
				else destinations = new int[]{source + 1, source - 1, source + 10, source - 10,
						source + 11, source - 11, source + 9, source - 9};
				for (int i = 0; i < destinations.length; i++) {
					if (destinations[i] == destination) {
						if (destination_square == GameData.EMPTY || destination_square < 0) {
							valid = true;
							break;
						}
					}
				}
				break;
			case GameData.PIECE_BISHOP:
			case GameData.PIECE_ROOK:
			case GameData.PIECE_QUEEN:
				int[] deltas = null;
				if (piece_value == GameData.PIECE_BISHOP) deltas = new int[]{11, -11, 9, -9};
				if (piece_value == GameData.PIECE_ROOK) deltas = new int[]{1, -1, 10, -10};
				if (piece_value == GameData.PIECE_QUEEN) deltas = new int[]{1, -1, 10, -10, 11, -11, 9, -9};
				for (int i = 0; i < deltas.length; i++) {
					int des = source + deltas[i];
					valid = true;
					while (destination != des) {
						destination_square = position.getBoard()[des];
						if (destination_square != GameData.EMPTY) {
							valid = false;
							break;
						}
						des += deltas[i];
					}
					if (valid) break;
				}
				break;
		}
		return valid;
	}

	public boolean checkCastling(int destination) {
		Piece king = this.getPosition().getHumanPieces()[8];
		Piece right_rook = this.getPosition().getHumanPieces()[6];
		Piece left_rook = this.getPosition().getHumanPieces()[5];

		if (king.isHasMoved()) return false;
		int source = move.getSourceLocation();

		if (right_rook == null && left_rook == null) return false;
		if (right_rook != null && right_rook.isHasMoved() &&
				left_rook != null && left_rook.isHasMoved()) return false;

		if (is_white) {
			if (source != 95) return false;
			if (destination != 97 && destination != 93) return false;
			if (destination == 97) {
				if (position.getBoard()[96] != GameData.EMPTY) return false;
				if (position.getBoard()[97] != GameData.EMPTY) return false;
				if (!game.safeMove(GameData.HUMAN, source, 96)) return false;
				if (!game.safeMove(GameData.HUMAN, source, 97)) return false;
			} else if (destination == 93) {
				if (position.getBoard()[94] != GameData.EMPTY) return false;
				if (position.getBoard()[93] != GameData.EMPTY) return false;
				if (!game.safeMove(GameData.HUMAN, source, 94)) return false;
				if (!game.safeMove(GameData.HUMAN, source, 93)) return false;
			}
		} else {
			if (source != 94) return false;
			if (destination != 92 && destination != 96) return false;
			if (destination == 92) {
				if (position.getBoard()[93] != GameData.EMPTY) return false;
				if (position.getBoard()[92] != GameData.EMPTY) return false;
				if (!game.safeMove(GameData.HUMAN, source, 93)) return false;
				if (!game.safeMove(GameData.HUMAN, source, 92)) return false;
			} else if (destination == 96) {
				if (position.getBoard()[95] != GameData.EMPTY) return false;
				if (position.getBoard()[96] != GameData.EMPTY) return false;
				if (!game.safeMove(GameData.HUMAN, source, 95)) return false;
				if (!game.safeMove(GameData.HUMAN, source, 96)) return false;
			}
		}
		return castling = true;
	}

	public int boardValue(int value) {
		return value / 45;
	}

	public void prepareAnimation() {
		int animating_image_key = 0;
		if (position.getBoard()[move.getSourceLocation()] > 0) {
			animating_image_key = position.getHumanPieces()[position.getBoard()[move.getSourceLocation()]].getValue();
		} else {
			animating_image_key = -position.getComputerPieces()[-position.getBoard()[move.getSourceLocation()]].getValue();
		}
		boardPane.animating_image = images.get(animating_image_key);
		int x = move.getSourceLocation() % 10;
		int y = (move.getSourceLocation() - x) / 10;
		boardPane.desX = move.getDestination() % 10;
		boardPane.desY = (move.getDestination() - boardPane.desX) / 10;
		int dX = boardPane.desX - x;
		int dY = boardPane.desY - y;
		boardPane.movingX = x * 45;
		boardPane.movingY = y * 45;
		if (Math.abs(dX) > Math.abs(dY)) {
			if (dY == 0) {
				boardPane.deltaX = (dX > 0) ? 1 : -1;
				boardPane.deltaY = 0;
			} else {
				boardPane.deltaX = (dX > 0) ? Math.abs(dX / dY) : -(Math.abs(dX / dY));
				boardPane.deltaY = (dY > 0) ? 1 : -1;
			}
		} else {
			if (dX == 0) {
				boardPane.deltaY = (dY > 0) ? 1 : -1;
				boardPane.deltaX = 0;
			} else {
				boardPane.deltaX = (dX > 0) ? 1 : -1;
				boardPane.deltaY = (dY > 0) ? Math.abs(dY / dX) : -(Math.abs(dY / dX));
			}
		}
		state = GameData.ANIMATING;
	}

	public void animate() {
		if (boardPane.movingX == boardPane.desX * 45 && boardPane.movingY == boardPane.desY * 45) {
			boardPane.repaint();
			int source_square = position.getBoard()[move.getSourceLocation()];
			if (source_square > 0) {
				state = GameData.COMPUTER_MOVE;
			} else {
				if (move.getDestination() > 90 && move.getDestination() < 98
						&& position.getComputerPieces()[-source_square].getValue() == GameData.PIECE_PAWN)
					promoteComputerPawn();
				//if(gameEnded(GameData.HUMAN)) state = GameData.GAME_ENDED;
				state = GameData.HUMAN_MOVE;
			}
			position.update(move);

			if (source_square > 0) {
				if (castling) {
					prepareCastlingAnimation();
					state = GameData.PREPARE_ANIMATION;
				} else if (move.getDestination() > 20 && move.getDestination() < 29 &&
						position.getHumanPieces()[source_square].getValue() == GameData.PIECE_PAWN) {
					promoteHumanPawn();
				}
			} else {
				if (gameEnded(GameData.HUMAN)) {
					state = GameData.GAME_ENDED;
					return;
				}
			}
			if (!castling && state != GameData.PROMOTING)
				newHistoryPosition();
			if (castling) {
				castling = false;
			}

		}
		boardPane.movingX += boardPane.deltaX;
		boardPane.movingY += boardPane.deltaY;
		boardPane.repaint();
	}

	public void promoteHumanPawn() {
		promotion_pane.setIntLocation(move.getDestination());
		promotion_pane.setIndex(position.getBoard()[move.getDestination()]);
		promotion_pane.setVisible(true);
		state = GameData.PROMOTING;
	}

	public void promoteComputerPawn() {
		int piece_index = position.getBoard()[move.getSourceLocation()];
		position.getComputerPieces()[-piece_index] = new Piece(GameData.PIECE_QUEEN, move.getDestination(), "QUEEN");
	}

	public void prepareCastlingAnimation() {
		if (move.getDestination() == 97 || move.getDestination() == 96) {
			move.setSourceLocation(98);
			move.setDestination(move.getDestination() - 1);
		} else if (move.getDestination() == 92 || move.getDestination() == 93) {
			move.setSourceLocation(91);
			move.setDestination(move.getDestination() + 1);
		}
	}

	public void newHistoryPosition() {
		history_positions.add(new Position(position));
		history_count = history_positions.size() - 1;
		historyBoardPane.repaint();
	}

	public void loadPieceImages() {
		char[] resource_keys = {'p', 'n', 'b', 'r', 'q', 'k'};
		int[] images_keys = {GameData.PIECE_PAWN, GameData.PIECE_KNIGHT, GameData.PIECE_BISHOP, GameData.PIECE_ROOK, GameData.PIECE_QUEEN, GameData.PIECE_KING};
		try {
			for (int i = 0; i < resource_keys.length; i++) {
				images.put(images_keys[i], ImageIO.read(this.resource.getResource((is_white ? "w" : "b") + resource_keys[i])));
				images.put(-images_keys[i], ImageIO.read(this.resource.getResource((is_white ? "b" : "w") + resource_keys[i])));
				images.put(images_keys[i] + 10, ImageIO.read(this.resource.getResource((is_white ? "w" : "b") + resource_keys[i] + '2')));
				images.put(-images_keys[i] + 10, ImageIO.read(this.resource.getResource((is_white ? "b" : "w") + resource_keys[i] + '2')));
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void loadBoardImages() {
		try {
			images.put(GameData.BOARD_IMAGE, ImageIO.read(this.resource.getResource("chessboard")));
			images.put(GameData.BOARD_IMAGE2, ImageIO.read(this.resource.getResource("history_board")));
			images.put(GameData.GLOW, ImageIO.read(this.resource.getResource("glow")));
			images.put(GameData.GLOW2, ImageIO.read(this.resource.getResource("glow2")));
			images.put(GameData.HISTORY_TITLE, ImageIO.read(this.resource.getResource("history_title")));
			images.put(GameData.CHESS, ImageIO.read(this.resource.getResource("chess")));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void loadMenuIcons() {
		iconImages.put(GameData.NEW_BUTTON, new ImageIcon(this.resource.getResource("new_game")));
		iconImages.put(GameData.NEW_BUTTON2, new ImageIcon(this.resource.getResource("new_game_hover")));
		iconImages.put(GameData.QUIT_BUTTON, new ImageIcon(this.resource.getResource("quit")));
		iconImages.put(GameData.QUIT_BUTTON2, new ImageIcon(this.resource.getResource("quit_hover")));
		iconImages.put(GameData.HISTORY_BUTTON, new ImageIcon(this.resource.getResource("history")));
		iconImages.put(GameData.HISTORY_BUTTON2, new ImageIcon(this.resource.getResource("history_hover")));
		iconImages.put(GameData.ABOUT_BUTTON, new ImageIcon(this.resource.getResource("about")));
		iconImages.put(GameData.ABOUT_BUTTON2, new ImageIcon(this.resource.getResource("about_hover")));

		iconImages.put(GameData.FIRST_BUTTON, new ImageIcon(this.resource.getResource("first")));
		iconImages.put(GameData.FIRST_BUTTON2, new ImageIcon(this.resource.getResource("first_hover")));
		iconImages.put(GameData.NEXT_BUTTON, new ImageIcon(this.resource.getResource("next")));
		iconImages.put(GameData.NEXT_BUTTON2, new ImageIcon(this.resource.getResource("next_hover")));
		iconImages.put(GameData.PREV_BUTTON, new ImageIcon(this.resource.getResource("previous")));
		iconImages.put(GameData.PREV_BUTTON2, new ImageIcon(this.resource.getResource("previous_hover")));
		iconImages.put(GameData.LAST_BUTTON, new ImageIcon(this.resource.getResource("last")));
		iconImages.put(GameData.LAST_BUTTON2, new ImageIcon(this.resource.getResource("last_hover")));
	}

	public void quit() {
		int option = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit?",
				"MainChess 1.1", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (option == JOptionPane.YES_OPTION)
			System.exit(0);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	//getters and setters
	public Position getPosition() {
		return this.position;
	}

	public ChessBoardPane getBoardPane() {
		return this.boardPane;
	}

	public void setIntState(int state) {
		this.state = state;
	}

	public class ChessBoardPane extends JPanel implements MouseListener {
		Image animating_image;
		int movingX, movingY, desX, desY, deltaX, deltaY;

		public ChessBoardPane() {
			setPreferredSize(new Dimension(450, 495));
			setBackground(backgroundColor);
			addMouseListener(this);
		}

		@Override
		public void paintComponent(Graphics g) {
			if (position.getBoard() == null) return;
			super.paintComponent(g);
			g.drawImage(images.get(GameData.CHESS), 20, 36, this);
			g.drawImage(images.get(GameData.BOARD_IMAGE), 20, 65, this);
			for (int i = 0; i < position.getBoard().length - 11; i++) {
				if (position.getBoard()[i] == GameData.ILLEGAL) continue;
				int x = i % 10;
				int y = (i - x) / 10;

				if (piece_selected && i == move.getSourceLocation()) {
					g.drawImage(images.get(GameData.GLOW), x * 45, y * 45, this);
				} else if (!piece_selected && move.getDestination() == i &&
						(position.getBoard()[i] == GameData.EMPTY || position.getBoard()[i] < 0)) {
					g.drawImage(images.get(GameData.GLOW2), x * 45, y * 45, this);
				}

				if (position.getBoard()[i] == GameData.EMPTY) continue;

				if (state == GameData.ANIMATING && i == move.getSourceLocation()) continue;
				if (position.getBoard()[i] > 0) {
					int piece = position.getHumanPieces()[position.getBoard()[i]].getValue();
					g.drawImage(images.get(piece), x * 45, y * 45, this);
				} else {
					int piece = position.getComputerPieces()[-position.getBoard()[i]].getValue();
					g.drawImage(images.get(-piece), x * 45, y * 45, this);
				}
			}
			if (state == GameData.ANIMATING) {
				g.drawImage(animating_image, movingX, movingY, this);
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (state != GameData.HUMAN_MOVE) return;
			int location = boardValue(e.getY()) * 10 + boardValue(e.getX());
			if (position.getBoard()[location] == GameData.ILLEGAL) return;
			if ((!piece_selected || position.getBoard()[location] > 0) && position.getBoard()[location] != GameData.EMPTY) {
				if (position.getBoard()[location] > 0) {
					piece_selected = true;
					move.setSourceLocation(location);
				}
			} else if (piece_selected && validMove(location)) {
				piece_selected = false;
				move.setDestination(location);
				state = GameData.PREPARE_ANIMATION;
			}
			repaint();
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}
	}

	public class HistoryBoardPane extends JPanel {
		public HistoryBoardPane() {
			setBackground(backgroundColor);
			setPreferredSize(new Dimension(300, 330));
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(images.get(GameData.HISTORY_TITLE), 20, 15, this);
			g.drawImage(images.get(GameData.BOARD_IMAGE2), 14, 44, this);
			if (history_positions.size() <= 0) return;
			Position _position = history_positions.get(history_count);
			for (int i = 0; i < _position.getBoard().length - 11; i++) {
				if (_position.getBoard()[i] == GameData.EMPTY) continue;
				if (_position.getBoard()[i] == GameData.ILLEGAL) continue;
				int x = i % 10;
				int y = (i - x) / 10;
				if (_position.getBoard()[i] > 0) {
					int piece = _position.getHumanPieces()[_position.getBoard()[i]].getValue();
					g.drawImage(images.get(piece + 10), x * 30, y * 30, this);
				} else {
					int piece = _position.getComputerPieces()[-_position.getBoard()[i]].getValue();
					g.drawImage(images.get(-piece + 10), x * 30, y * 30, this);
				}
			}
		}
	}

}
