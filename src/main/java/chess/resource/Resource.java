package chess.resource;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class Resource {
	private static ResourceBundle resources;

	static {
		try {
			resources = ResourceBundle.getBundle("chessProperties", Locale.getDefault());
		} catch (Exception e) {
			System.out.println("Chess properties not found");
			javax.swing.JOptionPane.showMessageDialog(null, "MainChess properties not found", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);

			System.exit(1);
		}
	}

	public String getResourceString(String key) {
		String str;

		try {
			str = resources.getString(key);
		} catch (Exception e) {
			str = null;
		}

		return str;
	}

	public URL getResource(String key) {

		String name = getResourceString(key);

		if (name != null) {
			return this.getClass().getResource(name);
		}

		return null;
	}
}
