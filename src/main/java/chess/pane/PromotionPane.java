package chess.pane;

import chess.MainChess;
import chess.game.GameData;
import chess.piece.Piece;
import chess.resource.Resource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PromotionPane extends JDialog implements ActionListener {
	private int index;
	private int location;
	private JPanel mainPane;
	private MainChess mainChess;

	public PromotionPane(MainChess mainChess) {
		setTitle("New Piece");
		this.setMainChess(mainChess);
		this.setMainPane(new JPanel(new GridLayout(1, 4, 10, 0)));

		int[] cmdActions = {
				GameData.PIECE_QUEEN, GameData.PIECE_ROOK, GameData.PIECE_BISHOP, GameData.PIECE_KNIGHT
		};

		for (int i = 0; i < cmdActions.length; i++) {
			JButton button = new JButton();
			button.addActionListener(this);
			button.setActionCommand(cmdActions[i] + "");
			this.getMainPane().add(button);
		}

		setContentPane(this.getMainPane());
		setResizable(true);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				resumeGame(GameData.PIECE_QUEEN);
			}
		});
	}

	public void setIcons(boolean white) {
		Component[] components = this.getMainPane().getComponents();
		Resource resource = new Resource();
		String[] resourceStrings = {"q", "r", "b", "n"};

		for (int i = 0; i < components.length; i++) {
			JButton button = (JButton) components[i];
			button.setIcon(new ImageIcon(resource.getResource((white ? "w" : "b") + resourceStrings[i])));
		}

		pack();
		setLocationRelativeTo(null);
	}

	public void actionPerformed(ActionEvent e) {
		int promotion_piece = Integer.parseInt(e.getActionCommand());
		setVisible(false);
		resumeGame(promotion_piece);
	}

	private void resumeGame(int promotion_piece) {
		this.getMainChess().getPosition().getHumanPieces()[this.getIndex()] = new Piece(promotion_piece, this.getIntLocation(), "PROM");
		this.getMainChess().newHistoryPosition();
		this.getMainChess().getBoardPane().repaint();
		this.getMainChess().setIntState(GameData.COMPUTER_MOVE);
	}

	//getters and setters
	private int getIndex() {
		return this.index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	private int getIntLocation() {
		return this.location;
	}

	public void setIntLocation(int location) {
		this.location = location;
	}

	private JPanel getMainPane() {
		return this.mainPane;
	}

	private void setMainPane(JPanel mainPane) {
		this.mainPane = mainPane;
	}

	private MainChess getMainChess() {
		return this.mainChess;
	}

	private void setMainChess(MainChess mainChess) {
		this.mainChess = mainChess;
	}
}
