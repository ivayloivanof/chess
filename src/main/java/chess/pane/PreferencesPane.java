package chess.pane;

import chess.MainChess;
import chess.game.GameData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PreferencesPane extends JFrame implements ActionListener {
	private JSlider levelSlider;
	private JRadioButton whiteButton;
	private JRadioButton blackButton;
	private JButton ok;
	private JButton cancel;
	private MainChess mainChess;

	public PreferencesPane(MainChess mainChess) {
		super("Options");
		this.setMainChess(mainChess);

		JPanel mainPane = new JPanel(new BorderLayout());
		mainPane.add(createLevelPane(), BorderLayout.NORTH);
		mainPane.add(createColorPane(), BorderLayout.CENTER);
		mainPane.add(createButtonPane(), BorderLayout.SOUTH);

		mainPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(mainPane);
		pack();
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		ok.addActionListener(this);
		cancel.addActionListener(this);
	}

	private JPanel createLevelPane() {
		this.setLevelSlider(new JSlider(JSlider.HORIZONTAL, 1, 5, 4));
		this.levelSlider.setMajorTickSpacing(1);
		this.levelSlider.setPaintTicks(true);
		this.levelSlider.setPaintLabels(true);

		JPanel levelPane = new JPanel();
		levelPane.add(this.levelSlider);
		levelPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), BorderFactory.createTitledBorder("Level")));

		return levelPane;
	}

	private JPanel createColorPane() {
		this.setWhiteButton(new JRadioButton("White", true));
		this.setBlackButton(new JRadioButton("Black"));

		ButtonGroup group = new ButtonGroup();
		group.add(this.whiteButton);
		group.add(this.blackButton);

		JPanel colorPane = new JPanel(new GridLayout(1, 2));
		colorPane.add(whiteButton);
		colorPane.add(blackButton);
		colorPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), BorderFactory.createTitledBorder("Color")));

		return colorPane;
	}

	private JPanel createButtonPane() {
		JPanel pane = new JPanel(new GridLayout(1, 2, 5, 0));
		pane.add(ok = new JButton("OK"));
		pane.add(cancel = new JButton("Cancel"));

		JPanel buttonPane = new JPanel(new BorderLayout());
		buttonPane.add(pane, BorderLayout.EAST);
		buttonPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		return buttonPane;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == ok) {
			this.getMainChess().setIntState(GameData.GAME_ENDED);
			this.getMainChess().newGame();
		}

		setVisible(false);
	}

	public JSlider getLevelSlider() {
		return this.levelSlider;
	}

	private void setLevelSlider(JSlider levelSlider) {
		this.levelSlider = levelSlider;
	}

	public JRadioButton getWhiteButton() {
		return this.whiteButton;
	}

	private void setWhiteButton(JRadioButton whiteButton) {
		this.whiteButton = whiteButton;
	}

	private void setBlackButton(JRadioButton blackButton) {
		this.blackButton = blackButton;
	}

	private MainChess getMainChess() {
		return this.mainChess;
	}

	private void setMainChess(MainChess mainChess) {
		this.mainChess = mainChess;
	}
}
