package chess.piece;

public class Piece {
	//TODO create inheritance
	private int value;
	private int location;
	private boolean hasMoved;
	private String type;

	public Piece(int value, int location, String type) {
		this(value, location, false);
		this.setType(type);
	}

	public Piece(int value, int location, boolean hasMoved) {
		this.setValue(value);
		this.setLocation(location);
		this.setHasMoved(hasMoved);
	}

	@SuppressWarnings("MethodDoesntCallSuperMethod")
	@Override
	public Piece clone() {
		return new Piece(this.getValue(), this.getLocation(), this.isHasMoved());
	}

	public int getValue() {
		return this.value;
	}

	private void setValue(int value) {
		this.value = value;
	}

	public int getLocation() {
		return this.location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public boolean isHasMoved() {
		return this.hasMoved;
	}

	public void setHasMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}

	public String getType() {
		return this.type;
	}

	private void setType(String type) {
		this.type = type;
	}
}
