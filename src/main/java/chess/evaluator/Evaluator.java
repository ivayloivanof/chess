package chess.evaluator;

import chess.game.GameData;
import chess.piece.Piece;
import chess.position.Position;

public class Evaluator {
	public int evaluate(Position position) {
		int human_score = 0;
		int computer_score = 0;
		for (int i = 1; i < position.getHumanPieces().length; i++) {
			if (position.getHumanPieces()[i] != null) {
				Piece piece = position.getHumanPieces()[i];
				int value = piece.getValue();
				human_score += value;
				int location = piece.getLocation();
				int col = (location % 10) - 1;
				int row = ((location - col) / 10) - 2;
				switch (value) {
					case GameData.PIECE_PAWN:
						if (row < 4) human_score += (8 - row);
						if (col > 4) {
							human_score -= ((col - 4));
						} else if (col < 3) {
							human_score -= ((3 - col));
						}
						if (col > 1 && col < 6 && row > 1) human_score += 2;
						if (row == 0) human_score += GameData.PIECE_QUEEN;
						//human_score += GameData.HUMAN_PAWN_TABLE[GameData.PIECE_location];
						break;
					case GameData.PIECE_KNIGHT:
						if (row == 7) human_score -= 1;
						if (col == 0 || col == 7) human_score -= 1;
						if (col > 1 && col < 6 && row > 1 && row < 6) human_score += 1;
						//human_score += GameData.HUMAN_KNIGHT_TABLE[GameData.PIECE_location];
						break;
					case GameData.PIECE_BISHOP:
						if (row == 7) human_score -= 1;
						if (col == 0 || col == 7) human_score -= 1;
						if (col > 0 && col < 7 && row > 0 && row < 7) human_score += 1;
						//human_score += GameData.HUMAN_BISHOP_TABLE[piece.location];
						break;
				}
			}
			if (position.getComputerPieces()[i] != null) {
				Piece piece = position.getComputerPieces()[i];
				int value = piece.getValue();
				computer_score += value;
				int location = piece.getLocation();
				int col = (location % 10) - 1;
				int row = ((location - col) / 10) - 2;
				switch (value) {
					case GameData.PIECE_PAWN:
						if (row > 3) computer_score += row;
						if (col > 4) {
							computer_score -= ((col - 4));
						} else if (col < 3) {
							computer_score -= ((3 - col));
						}
						if (col > 1 && col < 6 && row > 1) computer_score += 2;
						if (row == 7) computer_score += GameData.PIECE_QUEEN;
						//computer_score += GameData.COMPUTER_PAWN_TABLE[piece.location];
						break;
					case GameData.PIECE_KNIGHT:
						if (row == 0) computer_score -= 1;
						if (col == 0 || col == 7) computer_score -= 1;
						if (col > 1 && col < 6 && row > 1 && row < 6) computer_score += 1;
						//computer_score += GameData.COMPUTER_KNIGHT_TABLE[piece.location];
						break;
					case GameData.PIECE_BISHOP:
						if (row == 0) computer_score -= 1;
						if (col == 0 || col == 7) computer_score -= 1;
						if (col > 0 && col < 7 && row > 0 && row < 7) computer_score += 1;
						//computer_score += GameData.COMPUTER_BISHOP_TABLE[piece.location];
						break;
				}
			}
		}

		return human_score - computer_score;
	}
}
