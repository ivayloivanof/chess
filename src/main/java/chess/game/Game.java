package chess.game;

import chess.move.Move;
import chess.move.MoveGenerator;
import chess.piece.Piece;
import chess.position.Position;

public class Game {
	private Position position;
	private Piece playerKing;
	private Piece computerKing;

	public Game(Position position) {
		this.playerKing = position.getHumanPieces()[8];
		this.computerKing = position.getComputerPieces()[8];
		this.position = position;
	}

	public int getResult(int player) {
		int state = -1;
		MoveGenerator mg = new MoveGenerator(position, player);
		mg.generateMoves();
		Position[] positions = mg.getPositions();
		if (positions.length == 0) {
			if (isChecked(player)) {
				state = GameData.CHECKMATE;
			} else state = GameData.DRAW;
		}

		return state;
	}

	public boolean safeMove(int player, int source, int destination) {
		Move _move = new Move(source, destination);
		Position _position = new Position(this.position, _move);
		Game gs = new Game(_position);

		return !gs.isChecked(player);
	}

	public boolean isChecked(int player) {
		boolean checked = false;
		Piece king = (player == GameData.HUMAN) ? this.playerKing : this.computerKing;
		if (king == null) return false;
		checked = checkedByPawn(king);
		if (!checked) {
			checked = checkedByKnight(king);
		}
		if (!checked) {
			checked = checkedByBishop(king);
		}
		if (!checked) {
			checked = checkedByRook(king);
		}
		if (!checked) {
			checked = checkedByQueen(king);
		}
		if (!checked) {
			checked = desSquareAttackedByKing(king);
		}

		return checked;
	}

	private boolean checkedByPawn(Piece king) {
		boolean checked = false;
		int location = king.getLocation();
		if (king == this.playerKing) {
			int right_square = this.position.getBoard()[location - 9];
			int left_square = this.position.getBoard()[location - 11];
			if (right_square == GameData.ILLEGAL || left_square == GameData.ILLEGAL) return false;
			if (right_square < 0 && this.position.getComputerPieces()[-right_square].getValue() == GameData.PIECE_PAWN)
				checked = true;
			if (left_square < 0 && this.position.getComputerPieces()[-left_square].getValue() == GameData.PIECE_PAWN)
				checked = true;
		} else {
			int right_square = this.position.getBoard()[location + 11];
			int left_square = this.position.getBoard()[location + 9];
			if (right_square != GameData.ILLEGAL) {
				if (right_square > 0 && right_square != GameData.EMPTY &&
						this.position.getHumanPieces()[right_square].getValue() == GameData.PIECE_PAWN) {
					checked = true;
				}
			}
			if (left_square != GameData.ILLEGAL) {
				if (left_square > 0 && left_square != GameData.EMPTY &&
						this.position.getHumanPieces()[left_square].getValue() == GameData.PIECE_PAWN) {
					checked = true;
				}
			}
		}

		return checked;
	}

	private boolean checkedByKnight(Piece king) {
		boolean checked = false;
		int location = king.getLocation();
		int[] destinations = {location - 21, location + 21, location + 19, location - 19,
				location - 12, location + 12, location - 8, location + 8};
		for (int destination : destinations) {
			int des_square = this.position.getBoard()[destination];
			if (des_square == GameData.ILLEGAL) continue;
			if (king == this.playerKing) {
				if (des_square < 0 && this.position.getComputerPieces()[-des_square].getValue() == GameData.PIECE_KNIGHT) {
					checked = true;
					break;
				}
			} else {
				if (des_square > 0 && des_square != GameData.EMPTY &&
						this.position.getHumanPieces()[des_square].getValue() == GameData.PIECE_KNIGHT) {
					checked = true;
					break;
				}
			}
		}

		return checked;
	}

	private boolean desSquareAttackedByKing(Piece king) {
		boolean checked = false;
		int location = king.getLocation();
		int[] destinations = {location + 1, location - 1, location + 10, location - 10,
				location + 11, location - 11, location + 9, location - 9};
		for (int destination : destinations) {
			int des_square = this.position.getBoard()[destination];
			if (des_square == GameData.ILLEGAL) continue;
			if (king == this.playerKing) {
				if (des_square < 0 && position.getComputerPieces()[-des_square].getValue() == GameData.PIECE_KING) {
					checked = true;
					break;
				}
			} else {
				if (des_square > 0 && des_square != GameData.EMPTY &&
						this.position.getHumanPieces()[des_square].getValue() == GameData.PIECE_KING) {
					checked = true;
					break;
				}
			}
		}

		return checked;
	}

	private boolean checkedByBishop(Piece king) {
		boolean checked = false;
		int[] deltas = {11, -11, 9, -9};
		for (int i = 0; i < deltas.length; i++) {
			int delta = king.getLocation() + deltas[i];
			while (true) {
				int des_square = this.position.getBoard()[delta];
				if (des_square == GameData.ILLEGAL) {
					checked = false;
					break;
				}
				if (king == this.playerKing) {
					if (des_square < 0 && this.position.getComputerPieces()[-des_square].getValue() == GameData.PIECE_BISHOP) {
						checked = true;
						break;
					} else if (des_square != GameData.EMPTY) break;
				} else if (king == this.computerKing) {
					if (des_square > 0 && des_square != GameData.EMPTY &&
							this.position.getHumanPieces()[des_square].getValue() == GameData.PIECE_BISHOP) {
						checked = true;
						break;
					} else if (des_square != GameData.EMPTY) break;
				}
				delta += deltas[i];
			}
			if (checked) break;
		}

		return checked;
	}

	private boolean checkedByRook(Piece king) {
		boolean checked = false;
		int[] deltas = {1, -1, 10, -10};
		for (int i = 0; i < deltas.length; i++) {
			int delta = king.getLocation() + deltas[i];
			while (true) {
				int des_square = this.position.getBoard()[delta];
				if (des_square == GameData.ILLEGAL) {
					checked = false;
					break;
				}
				if (king == this.playerKing) {
					if (des_square < 0 && position.getComputerPieces()[-des_square].getValue() == GameData.PIECE_ROOK) {
						checked = true;
						break;
					} else if (des_square != GameData.EMPTY) break;
				} else if (king == this.computerKing) {
					if (des_square > 0 && des_square != GameData.EMPTY &&
							this.position.getHumanPieces()[des_square].getValue() == GameData.PIECE_ROOK) {
						checked = true;
						break;
					} else if (des_square != GameData.EMPTY) break;
				}
				delta += deltas[i];
			}
			if (checked) break;
		}

		return checked;
	}

	private boolean checkedByQueen(Piece king) {
		boolean checked = false;
		int[] deltas = {1, -1, 10, -10, 11, -11, 9, -9};
		for (int i = 0; i < deltas.length; i++) {
			int delta = king.getLocation() + deltas[i];
			while (true) {
				int des_square = this.position.getBoard()[delta];
				if (des_square == GameData.ILLEGAL) {
					checked = false;
					break;
				}
				if (king == this.playerKing) {
					if (des_square < 0 && this.position.getComputerPieces()[-des_square].getValue() == GameData.PIECE_QUEEN) {
						checked = true;
						break;
					} else if (des_square != GameData.EMPTY) break;
				} else if (king == this.computerKing) {
					if (des_square > 0 && des_square != GameData.EMPTY &&
							this.position.getHumanPieces()[des_square].getValue() == GameData.PIECE_QUEEN) {
						checked = true;
						break;
					} else if (des_square != GameData.EMPTY) break;
				}
				delta += deltas[i];
			}
			if (checked) break;
		}

		return checked;
	}
}
