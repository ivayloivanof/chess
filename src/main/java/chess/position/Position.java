package chess.position;

import chess.game.GameData;
import chess.move.Move;
import chess.piece.Piece;

public class Position {

	private Piece[] humanPieces = new Piece[17];
	private Move lastMove;
	private int[] board = new int[120];
	private Piece[] computerPieces = new Piece[17];

	public Position() {
		for (int i = 0; i < this.board.length; i++) {
			this.board[i] = GameData.EMPTY;
		}
	}

	public Position(Position position) {
		this(position, null);
	}

	public Position(Position position, Move lastMove) {
		System.arraycopy(position.board, 0, this.board, 0, this.board.length);
		for (int i = 1; i < this.humanPieces.length; i++) {
			if (position.humanPieces[i] != null) {
				this.humanPieces[i] = position.humanPieces[i].clone();
			}

			if (position.computerPieces[i] != null) {
				this.computerPieces[i] = position.computerPieces[i].clone();
			}
		}

		if (lastMove != null) {
			update(lastMove);
		}
	}

	public void initialize(boolean humanWhite) {
		this.humanPieces[1] = new Piece(GameData.PIECE_KNIGHT, 92, "KNIGHT");
		this.humanPieces[2] = new Piece(GameData.PIECE_KNIGHT, 97, "KNIGHT");
		this.humanPieces[3] = new Piece(GameData.PIECE_BISHOP, 93, "BISHOP");
		this.humanPieces[4] = new Piece(GameData.PIECE_BISHOP, 96, "BISHOP");
		this.humanPieces[5] = new Piece(GameData.PIECE_ROOK, 91, "ROOK");
		this.humanPieces[6] = new Piece(GameData.PIECE_ROOK, 98, "ROOK");
		this.humanPieces[7] = new Piece(GameData.PIECE_QUEEN, humanWhite ? 94 : 95, "QUEEN");
		this.humanPieces[8] = new Piece(GameData.PIECE_KING, humanWhite ? 95 : 94, "KING");

		this.computerPieces[1] = new Piece(GameData.PIECE_KNIGHT, 22, "KNIGHT");
		this.computerPieces[2] = new Piece(GameData.PIECE_KNIGHT, 27, "KNIGHT");
		this.computerPieces[3] = new Piece(GameData.PIECE_BISHOP, 23, "BISHOP");
		this.computerPieces[4] = new Piece(GameData.PIECE_BISHOP, 26, "BISHOP");
		this.computerPieces[5] = new Piece(GameData.PIECE_ROOK, 21, "ROOK");
		this.computerPieces[6] = new Piece(GameData.PIECE_ROOK, 28, "ROOK");
		this.computerPieces[7] = new Piece(GameData.PIECE_QUEEN, humanWhite ? 24 : 25, "QUEEN");
		this.computerPieces[8] = new Piece(GameData.PIECE_KING, humanWhite ? 25 : 24, "KING");

		int j = 81;
		for (int i = 9; i < this.humanPieces.length; i++) {
			this.humanPieces[i] = new Piece(GameData.PIECE_PAWN, j, "PAWN");
			this.computerPieces[i] = new Piece(GameData.PIECE_PAWN, j - 50, "PAWN");
			j++;
		}

		this.setBoard(new int[]{
				GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.EMPTY, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL,
				GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL, GameData.ILLEGAL
		});

		for (int i = 0; i < this.board.length; i++) {
			for (int k = 1; k < this.humanPieces.length; k++) {
				if (i == this.humanPieces[k].getLocation()) {
					this.board[i] = k;
				} else if (i == this.computerPieces[k].getLocation()) {
					this.board[i] = -k;
				}
			}
		}
	}

	//move figure
	public void update(Move move) {
		this.lastMove = move;
		int sourceIndex = this.board[move.getSourceLocation()];
		int destinationIndex = this.board[move.getDestination()];

		if (sourceIndex > 0) {
			this.humanPieces[sourceIndex].setHasMoved(true);
			this.humanPieces[sourceIndex].setLocation(move.getDestination());
			if (destinationIndex < 0) {
				this.computerPieces[-destinationIndex] = null;
			}
		} else {
			this.computerPieces[-sourceIndex].setHasMoved(true);
			this.computerPieces[-sourceIndex].setLocation(move.getDestination());
			if (destinationIndex > 0 && destinationIndex != GameData.EMPTY) {
				this.humanPieces[destinationIndex] = null;
			}
		}

		this.board[move.getSourceLocation()] = GameData.EMPTY;
		this.board[move.getDestination()] = sourceIndex;
	}

	//getters and setters
	public Piece[] getHumanPieces() {
		return this.humanPieces;
	}

	public Move getLastMove() {
		return this.lastMove;
	}

	public int[] getBoard() {
		return this.board;
	}

	private void setBoard(int[] board) {
		this.board = board;
	}

	public Piece[] getComputerPieces() {
		return this.computerPieces;
	}

}
