package chess.move;

public class Move {
	private int sourceLocation;
	private int destination;

	public Move() {
		setSourceLocation(-1);
		setDestination(-1);
	}

	public Move(int source_location, int destination) {
		this.sourceLocation = source_location;
		this.destination = destination;
	}

	public int getSourceLocation() {
		return this.sourceLocation;
	}

	public void setSourceLocation(int sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public int getDestination() {
		return this.destination;
	}

	public void setDestination(int destination) {
		this.destination = destination;
	}
}
