package chess.move;

import chess.game.Game;
import chess.game.GameData;
import chess.piece.Piece;
import chess.position.Position;

import java.util.ArrayList;
import java.util.List;

public class MoveGenerator {

	private Position position;
	private int player;
	private List<Position> positions = new ArrayList<Position>();
	private Game gs;

	public MoveGenerator(Position position, int player) {
		this.position = position;
		this.player = player;
		this.gs = new Game(position);
	}

	public void generateMoves() {
		if (player == GameData.HUMAN) {
			for (int i = 1; i < position.getHumanPieces().length; i++) {
				Piece piece = position.getHumanPieces()[i];
				if (piece == null) continue;
				switch (piece.getValue()) {
					case GameData.PIECE_PAWN:
						humanPawnMoves(piece);
						break;
					case GameData.PIECE_KNIGHT:
						humanKnightMoves(piece);
						break;
					case GameData.PIECE_KING:
						humanKingMoves(piece);
						break;
					case GameData.PIECE_BISHOP:
						humanBishopMoves(piece);
						break;
					case GameData.PIECE_ROOK:
						humanRookMoves(piece);
						break;
					case GameData.PIECE_QUEEN:
						humanQueenMoves(piece);
				}
			}
		} else {
			for (int i = 1; i < position.getComputerPieces().length; i++) {
				Piece piece = position.getComputerPieces()[i];
				if (piece == null) continue;
				switch (piece.getValue()) {
					case GameData.PIECE_PAWN:
						computerPawnMoves(piece);
						break;
					case GameData.PIECE_KNIGHT:
						computerKnightMoves(piece);
						break;
					case GameData.PIECE_KING:
						computerKingMoves(piece);
						break;
					case GameData.PIECE_BISHOP:
						computerBishopMoves(piece);
						break;
					case GameData.PIECE_ROOK:
						computerRookMoves(piece);
						break;
					case GameData.PIECE_QUEEN:
						computerQueenMoves(piece);
				}
			}
		}
	}

	private void humanPawnMoves(Piece pawn) {
		int location = pawn.getLocation();
		int forward_piece_index = position.getBoard()[location - 10];
		if (forward_piece_index != GameData.ILLEGAL) {
			if (forward_piece_index == GameData.EMPTY && gs.safeMove(GameData.HUMAN, location, location - 10)) {
				positions.add(new Position(position, new Move(location, location - 10)));
			}
		}

		if (location > 80 && forward_piece_index == GameData.EMPTY &&
				position.getBoard()[location - 20] == GameData.EMPTY && gs.safeMove(GameData.HUMAN, location, location - 20)) {
			positions.add(new Position(position, new Move(location, location - 20)));
		}

		int right_piece_index = position.getBoard()[location - 9];
		if (right_piece_index != GameData.ILLEGAL) {
			if (right_piece_index < 0 && gs.safeMove(GameData.HUMAN, location, location - 9))
				positions.add(new Position(position, new Move(location, location - 9)));
		}
		int left_piece_index = position.getBoard()[location - 11];
		if (left_piece_index != GameData.ILLEGAL) {
			if (left_piece_index < 0 && gs.safeMove(GameData.HUMAN, location, location - 11))
				positions.add(new Position(position, new Move(location, location - 11)));
		}
	}

	private void computerPawnMoves(Piece pawn) {
		int location = pawn.getLocation();
		int forward_piece_index = position.getBoard()[location + 10];
		if (forward_piece_index != GameData.ILLEGAL) {
			if (forward_piece_index == GameData.EMPTY && gs.safeMove(GameData.COMPUTER, location, location + 10)) {
				positions.add(new Position(position, new Move(location, location + 10)));
			}
		}

		if (location < 39 && forward_piece_index == GameData.EMPTY &&
				position.getBoard()[location + 20] == GameData.EMPTY && gs.safeMove(GameData.COMPUTER, location, location + 20)) {
			positions.add(new Position(position, new Move(location, location + 20)));
		}

		int right_piece_index = position.getBoard()[location + 11];
		if (right_piece_index != GameData.ILLEGAL) {
			if (right_piece_index > 0 && right_piece_index != GameData.EMPTY &&
					gs.safeMove(GameData.COMPUTER, location, location + 11))
				positions.add(new Position(position, new Move(location, location + 11)));
		}
		int left_piece_index = position.getBoard()[location + 9];
		if (left_piece_index != GameData.ILLEGAL) {
			if (left_piece_index > 0 && left_piece_index != GameData.EMPTY &&
					gs.safeMove(GameData.COMPUTER, location, location + 9))
				positions.add(new Position(position, new Move(location, location + 9)));
		}
	}

	private void humanKnightMoves(Piece knight) {
		int location = knight.getLocation();
		int[] destinations = {location - 21, location + 21, location + 19, location - 19,
				location - 12, location + 12, location - 8, location + 8};
		for (int i = 0; i < destinations.length; i++) {
			int des_piece_index = position.getBoard()[destinations[i]];
			if (des_piece_index != GameData.ILLEGAL && (des_piece_index == GameData.EMPTY || des_piece_index < 0)
					&& gs.safeMove(GameData.HUMAN, location, destinations[i]))
				positions.add(new Position(position, new Move(location, destinations[i])));
		}
	}

	private void computerKnightMoves(Piece knight) {
		int location = knight.getLocation();
		int[] destinations = {location - 21, location + 21, location + 19, location - 19,
				location - 12, location + 12, location - 8, location + 8};
		for (int i = 0; i < destinations.length; i++) {
			int des_piece_index = position.getBoard()[destinations[i]];
			if (des_piece_index != GameData.ILLEGAL && (des_piece_index == GameData.EMPTY || des_piece_index > 0) &&
					gs.safeMove(GameData.COMPUTER, location, destinations[i])) {
				positions.add(new Position(position, new Move(location, destinations[i])));
			}
		}
	}

	private void humanKingMoves(Piece king) {
		int location = king.getLocation();
		int[] destinations = {location + 1, location - 1, location + 10, location - 10,
				location + 11, location - 11, location + 9, location - 9};
		for (int i = 0; i < destinations.length; i++) {
			int des_piece_index = position.getBoard()[destinations[i]];
			if (des_piece_index != GameData.ILLEGAL && (des_piece_index == GameData.EMPTY || des_piece_index < 0)
					&& gs.safeMove(GameData.HUMAN, location, destinations[i])) {
				positions.add(new Position(position, new Move(location, destinations[i])));
			}
		}
	}

	private void computerKingMoves(Piece king) {
		int location = king.getLocation();
		int[] destinations = {location + 1, location - 1, location + 10, location - 10,
				location + 11, location - 11, location + 9, location - 9};
		for (int i = 0; i < destinations.length; i++) {
			int des_piece_index = position.getBoard()[destinations[i]];
			if (des_piece_index != GameData.ILLEGAL && (des_piece_index == GameData.EMPTY || des_piece_index > 0) &&
					gs.safeMove(GameData.COMPUTER, location, destinations[i])) {
				positions.add(new Position(position, new Move(location, destinations[i])));
			}
		}
	}

	private void humanBishopMoves(Piece bishop) {
		int location = bishop.getLocation();
		int[] deltas = {11, -11, 9, -9};
		for (int i = 0; i < deltas.length; i++) {
			int des = location + deltas[i];
			while (true) {
				int des_piece_index = position.getBoard()[des];
				if (des_piece_index == GameData.ILLEGAL) {
					break;
				}
				boolean safe_move = gs.safeMove(GameData.HUMAN, location, des);
				if (des_piece_index == GameData.EMPTY || des_piece_index < 0) {
					if (safe_move) {
						positions.add(new Position(position, new Move(location, des)));
						if (des_piece_index != GameData.EMPTY || !safe_move) {
							break;
						}
					} else if (des_piece_index != GameData.EMPTY) break;
				} else if (des_piece_index > 0 && des_piece_index != GameData.EMPTY) {
					break;
				}
				des += deltas[i];
			}
		}
	}

	private void computerBishopMoves(Piece bishop) {
		int location = bishop.getLocation();
		int[] deltas = {11, -11, 9, -9};
		for (int i = 0; i < deltas.length; i++) {
			int des = location + deltas[i];
			while (true) {
				int des_piece_index = position.getBoard()[des];
				if (des_piece_index == GameData.ILLEGAL) {
					break;
				}
				boolean safe_move = gs.safeMove(GameData.COMPUTER, location, des);
				if (des_piece_index == GameData.EMPTY || des_piece_index > 0) {
					if (safe_move) {
						positions.add(new Position(position, new Move(location, des)));
						if (des_piece_index != GameData.EMPTY || !safe_move) {
							break;
						}
					} else if (des_piece_index != GameData.EMPTY) break;
				} else if (des_piece_index < 0) {
					break;
				}
				des += deltas[i];
			}
		}
	}

	private void humanRookMoves(Piece rook) {
		int location = rook.getLocation();
		int[] deltas = {1, -1, 10, -10};
		humanMove(location, deltas);
	}

	private void computerRookMoves(Piece rook) {
		int location = rook.getLocation();
		int[] deltas = {1, -1, 10, -10};
		computerMove(location, deltas);
	}

	private void humanQueenMoves(Piece queen) {
		int location = queen.getLocation();
		int[] deltas = {1, -1, 10, -10, 11, -11, 9, -9};
		humanMove(location, deltas);
	}

	private void computerQueenMoves(Piece queen) {
		int location = queen.getLocation();
		int[] deltas = {1, -1, 10, -10, 11, -11, 9, -9};
		computerMove(location, deltas);
	}

	private void computerMove(int location, int[] deltas) {
		for (int i = 0; i < deltas.length; i++) {
			int des = location + deltas[i];
			while (true) {
				int des_piece_index = position.getBoard()[des];
				if (des_piece_index == GameData.ILLEGAL) {
					break;
				}
				boolean safe_move = gs.safeMove(GameData.COMPUTER, location, des);
				if (des_piece_index == GameData.EMPTY || des_piece_index > 0) {
					if (safe_move) {
						positions.add(new Position(position, new Move(location, des)));
						if (des_piece_index != GameData.EMPTY) {
							break;
						}
					} else if (des_piece_index != GameData.EMPTY) break;
				} else if (des_piece_index < 0) {
					break;
				}
				des += deltas[i];
			}
		}
	}

	private void humanMove(int location, int[] deltas) {
		for (int i = 0; i < deltas.length; i++) {
			int des = location + deltas[i];
			while (true) {
				int des_piece_index = position.getBoard()[des];
				if (des_piece_index == GameData.ILLEGAL) {
					break;
				}
				boolean safe_move = gs.safeMove(GameData.HUMAN, location, des);
				if (des_piece_index == GameData.EMPTY || des_piece_index < 0) {
					if (safe_move) {
						positions.add(new Position(position, new Move(location, des)));
						if (des_piece_index != GameData.EMPTY) {
							break;
						}
					} else if (des_piece_index != GameData.EMPTY) break;
				} else if (des_piece_index > 0 && des_piece_index != GameData.EMPTY) {
					break;
				}
				des += deltas[i];
			}
		}
	}

	//getters and setters
	public Position[] getPositions() {
		return positions.toArray(new Position[positions.size()]);
	}
}
